## Synopsis

A simple employee directory project for PhoneGap - A mobile application development tool 

## Motivation

Having just picked up PhoneGap I decided I needed a sandbox app to learn on. This is it.


## Installation
If you have PhoneGap desktop then you can serve this project up that way (for development), otherwise you should build it on http://build.phonegap.com and download to your mobile device
